<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>FOOTER</title>
      <link rel="stylesheet" href="../css/footer.css">
      <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
<body>
  <section class="footer">
    <div class="content_services footer">
      <div class="container">
        <div class="row">
      <div class="col-5"><h2 class="h2_footer">JSME TADY PRO VÁS</h2></div>
      <div class="col-3"><h2 class="h2_footer">NAPIŠTE ČI ZAVOLEJTE</h2></div>
      <div class="col-4"></div>
    </div>
        </div>
        <div class="container">
          <div class="row">
        <div class="col-5 lorem2"><a class="lorem2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam neque. In enim a arcu imperdiet malesuada. Donec ipsum massa, ullamcorper in, auctor et, scelerisque sed, est.</a></div>
        <div class="col-3 lorem2"><a class="lorem2"></a>
          <div class="container">
            <div class="col tel_footer1"><i class="fas fa-mobile"></i> <a href="tel:602477338" class="tel_footer2"> +420 602 602 477 338</a></div>
            <div class="col mail_footer1"><i class="fas fa-envelope"></i ><a href="mailto:info@plynomontaz.cz" class="mail_footer2"> info@plynomontaz.cz</a></div>
          </div>
      </div>
                <div class="col-4 frame"><div class="fb-page" data-href="https://www.facebook.com/kadlecsoftware/" data-tabs="timeline" data-width="" data-height="100px" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kadlecsoftware/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kadlecsoftware/">Kadlec - Software</a></blockquote></div></div>
        </div>
          </div>

            <div class="under_footer">
            <div class="container">
              <div class="row">
                <div class="col-3 little_textos">@Plynomontáž.cz 2019, Provozuje Josef Kadlec</div>
                <div class="col-7"></div>
                <div class="col-2 little_textos">Vytvořil <a href="https://kadlec-software.cz/"><u>Kadlec-Software</u></a></div>
                </div>
            </div>
            </div>
      </div>
  </SECTION>

  <div id="fb-root"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v3.3"></script>

</body>
</html>
