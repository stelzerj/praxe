<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>CONTACTS</title>
      <link rel="stylesheet" href="../css/contact.css">
      <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  </head>
<body>
  <script>
  function validateForm() {
    var x = document.forms["form"]["name"].value;
    if (x == "") {
      alert("Name must be filled out");
      return false;
    }
  }
</script>
<form action="contact.php" method="post">
  <section class="services">
    <div class="content_services contact">
      <div class="container">
        <div class="row">
      <div class="col-12"><h2 class="h2_contact">POTŘEBUJETE SERVIS ČI OPRAVU?</h2></div>
    </div>
        <div class="row popisky">
          <div class="col-6"> <a class="nadpopisek">JMÉNO A PŘÍJMENÍ</a> </div>
          <div class="col-6"> <a class="nadpopisek">TELEFONNÍ ČÍSLO</a> </div>
        </div>
        <div class="row">

          <div class="col-6"><textarea name="name" class="textarea" placeholder="Např.: Jan Novák" required></textarea> </div>
          <div class="col-6"><textarea name="phone" class="textarea" placeholder="Např.: +420 602 477 338" required></textarea></div>
        </div>
        <div class="row odstup">
          <div class="col-12 popisek"> <a class="nadpopisek">POPIS ZÁVADY</a> </div>
        </div>
        <div class="row">
          <div class="col-12"><textarea name="servis" class="textarea2" placeholder="Zde prosím popiště závadu nebo vzneste dotaz..." required></textarea></div>
        </div>
        <div class="row">
          <div class="col-12 souhlas"><a>ODESLÁNÍM FORMULÁŘE SOUHLASÍTE SE ZPRACOVÁNÍM OSOBNÍCH ÚDAJŮ. VÍCE INFORMACÍ NALEZDENETE <b><u>ZDE.</b></u></a></div>
          <div class="col-3"><input type="submit" value="ODESLAT" class="button button2"></div>
        </div>
  </div>
</form>

  </div>
  </SECTION>

</body>
</html>
