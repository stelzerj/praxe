<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>PARTNERS</title>
  <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

 <link rel="stylesheet" href="../css/partners.css">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">



</head>
<body>
  <div class="content_services">
            <div class="container textos">
              <div class="row">
              <div class="col-12"><h2 class="h2_partners">PARTNERSTVÍ S JEDNIČKAMI V OBORU</h2></div>
              <div class="col-12"><a>Lorem ipsum dolor sir amet, consectetuer adipiscingelit. Prasent vitae arcu tempor neque lacinia pretium. In convallis.</a></div>
            </div>
            </div>
          </div>
<div class="swiper-container swiper1">
  <div class="swiper-wrapper">
    <div class="swiper-slide"><a href="#">  <img src="images/prothermlogo.png" height="95px" width="350px" alt=""> </a></div>
    <div class="swiper-slide"><a href="#">  <img src="images/Vaillant.png" alt=""></a> </div>
    <div class="swiper-slide"><a href="#">  <img src="images/viadrus.png" alt=""></a> </div>
    <div class="swiper-slide"><a href="#">  <img src="images/viessmann.png" alt=""></a> </div>
    <div class="swiper-slide"><a href="#">  <img src="images/junkers.png" alt="" height="300px" width="350px"></a> </div>
    <div class="swiper-slide"><a href="#">  <img src="images/buderus.png" alt=""></a> </div>
    <div class="swiper-slide"><a href="#">  <img src="images/thermona.png" alt=""></a> </div>
  </div>
  <div class="swiper-pagination"></div>
  <div class="swiper-button-next"></div>
  <div class="swiper-button-prev"></div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
  <script src="js/swiper_partners.js">
  </script>
</body>
</html>
