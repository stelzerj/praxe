<!DOCTYPE html>
<html lang="cs" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/css.css">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <title>HEADER</title>
  </head>
  <body>

      <nav class="header">

            <div class="banner">VE DNECH 20.6.019 AŽ 20.7.2019 PROBÍHA DOVOLENÁ, DĚKUJEME ZA POCHOPENÍ</div>
            <div class="container">
            <div class="row">
              <div class="col-3"> <img src="images/logo.png" height="90px" weight="60px"> </div>
              <div class="col-3"></div>
              <div class="col-1 idk"><a class="odkaz" href="">DOMŮ</a></div>

              <div class="col-1 idk"><a class="odkaz" href="">PARTNEŘI</a></div>

              <div class="col-1 idk"><a class="odkaz" href="">SLUŽBY</a></div>

              <div class="col-1 idk"><a class="odkaz" href="">KONTAKTY</a></div>

              <div class="col-2 idk"><a href="tel:602477338" class="tel">+420 602 477 338</a></div>
            </div>
          </div>
          </nav>
          <section class="slider">
            <div class="swiper-container">
              <div class="swiper-wrapper">
                <div class="swiper-slide"> <img src="images/slider3.png" height="608" weight="1110"> </div>
                <div class="swiper-slide"> <img src="images/slider2.jpg" height="608" weight="1500"></div>
                <div class="swiper-slide"> <img src="images/slider1.jpg" height="608" weight="1110"> </div>
              </div>

              <div class="swiper-pagination"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
            <script src="js/swiper.js"></script>
        </section>


  </body>
</html>
